/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { VisualExpertComponent } from './visual-expert.component';

describe('VisualExpertComponent', () => {
  let component: VisualExpertComponent;
  let fixture: ComponentFixture<VisualExpertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualExpertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualExpertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
