import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'play-visual-expert',
  template: `
  <h1>Visual Expert {{version}}</h1>

<hr>
<h2>Toolbar</h2>
<hr>
<md-toolbar [color]="myColor">
  <span>My Application Title</span>
</md-toolbar>

<hr>
<h2>Buttons</h2>
<hr>

<button md-button>FLAT</button>
<button md-raised-button>RAISED</button>
<button md-fab>
    <md-icon class="md-24">add</md-icon>
</button>
<button md-mini-fab>
    <md-icon class="md-24">add</md-icon>
</button>

<hr>
<h2>Listing</h2>
<hr>

<md-list>
  <md-list-item *ngFor="let message of messages">
    <h3 md-line> {{message.from}} </h3>
    <p md-line> {{message.subject}} </p>
    <p md-line class="demo-2"> {{message.message}} </p>
  </md-list-item>
</md-list>


<hr>
<h2>Cards</h2>
<hr>

<md-card *ngFor="let message of messages">
   <md-card-title #fromPerson>{{message.from}}</md-card-title>   
   <md-card-subtitle>{{message.subject}}</md-card-subtitle>
   <md-card-content>{{message.message}}</md-card-content>
   <md-card-actions>
        <button md-button (click)="onClick(fromPerson.innerHTML)">LIKE</button>
        <button md-button>SHARE</button>
   </md-card-actions>
</md-card>


  `,
  styles: [`
:host md-card{
}

  `]
})
export class VisualExpertComponent implements OnInit {
  
  version="v1.0.0";
  messages = [
    {from:"A", subject:"Subject", message:"This is a message"}
    , {from:"A", subject:"Subject", message:"This is a message"}
  ];

  constructor() { }

  ngOnInit() {
  }

  onClick(from) {
    console.log("Hello from:" + from );
  }
}
