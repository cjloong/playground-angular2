import { Component } from '@angular/core';


@Component({
  selector: 'play-root',
  template: `
<nav>
    <a routerLink="/visual-expert" routerLinkActive="active">Visual Expert</a>
    <a routerLink="/expert-shell" routerLinkActive="active">Shell</a>
  </nav>
  <router-outlet></router-outlet>    
  `,
  styles: [`
play-visual-expert {
  width: 100px;
}
  `]
})
export class AppComponent {
  title = 'play works!';
}
