import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'play-expert-shell',
  template: `
<header>Expert Shell {{version}}</header>
<pre class="terminaljs" data-columns="80" data-rows="24">
Terminal loading...
</pre>
  `,
  styles: [`
  `]
})
export class ExpertShellComponent implements OnInit {
  
  version="v1.0.0";
  constructor() { }
  ngOnInit() {
  }

  
}
