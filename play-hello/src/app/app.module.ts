import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { VisualExpertComponent } from './visual-expert/visual-expert.component';
import { ExpertShellComponent } from './expert-shell/expert-shell.component';
import { MaterialModule } from '@angular/material';


const appRoutes: Routes = [
  { path: '', component: ExpertShellComponent },
  { path: 'visual-expert', component: VisualExpertComponent },
  { path: 'expert-shell', component: ExpertShellComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    VisualExpertComponent,
      ExpertShellComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
