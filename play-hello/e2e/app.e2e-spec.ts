import { PlayHelloPage } from './app.po';

describe('play-hello App', function() {
  let page: PlayHelloPage;

  beforeEach(() => {
    page = new PlayHelloPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('play works!');
  });
});
