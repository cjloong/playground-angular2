import { browser, element, by } from 'protractor';

export class PlayHelloPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('play-root h1')).getText();
  }
}
