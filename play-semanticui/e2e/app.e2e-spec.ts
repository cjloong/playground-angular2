import { PlaySemanticuiPage } from './app.po';

describe('play-semanticui App', function() {
  let page: PlaySemanticuiPage;

  beforeEach(() => {
    page = new PlaySemanticuiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('xevo works!');
  });
});
