import { browser, element, by } from 'protractor';

export class PlaySemanticuiPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('xevo-root h1')).getText();
  }
}
