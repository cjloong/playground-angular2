import { Component } from '@angular/core';

@Component({
  selector: 'xevo-root',
  template: `
<xevo-main></xevo-main>
  `,
  styles: [`

  `]
})
export class AppComponent {
  title = 'xevo works!';
}
